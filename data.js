/* eslint max-len: ["off"] */
export const texts = [
  'Text for typing #1',
  'Text for typing #2',
  'Text for typing #3',
  'Text for typing #4',
  'Text for typing #5',
  'Text for typing #6',
  'Text for typing #7'
];

export const quoteTemplates = {
  greeting: [
    'На вулиці зараз трохи пасмурно, але на Львів Арена зараз просто чудова атмосфера: двигуни гарчать, глядачі посміхаються а гонщики ледь помітно нервують та готуюуть своїх залізних конів до заїзду. А коментувати це все дійство Вам буду я, {commentator_name} і я радий вас вітати зі словами Доброго Вам дня панове!',
    'Всім привіт, з вами український голос чемпіонату з клавагонок {commentator_name}. Гарного перегляду',
    'Доброго дня. Мене звати {commentator_name}. Поїхали!!!'
  ],
  introducing: [
    'А тим часом список гонщиків: на поул-позишин сьогодні у {first_player}<, разон із ним на стартовому ряду {second_player}><, третім йде {third_player}><, четвертий - {fourth_player}>< і замикає пелотон {fifth_player}>.',
    'Ось хто сьогодні буде змагатися за перемогу: {first_player}<, {second_player}><, {third_player}><, {fourth_player},><, {fifth_player}>.'
  ],
  gameStatus: [
    'Поки що маємо наступну ситуацію: 1-й {first_player}<, 2-й {second_player}><, 3-й {third_player}><, 4-й {fourth_player}><, 5-й {fifth_player}>.',
    'На першому місці зараз {first_player}<, за ним {second_player}>.',
    'А тим часом лідирує {first_player}. Дуже гарний пілотаж.',
    '<{second_player} не здається і бореться за перше місце.>',
    '<На даний момент {third_player} займає третю позицію.>',
    '<У {fifth_player} сьогодні зовсім не вдалася гонка.>'
  ],
  finishLine: [
    'До фінішу залишилось зовсім небагато і схоже що першим його може перетнути {first_player}.',
    'Фінальний відрізок гри. {first_player} дуже близький до перемоги<, але {second_player} не здається>.',
    'Залишається зовсім трішки до завершення гонки<. Друге місце може залишитись {second_player}>< чи {third_player}>.'
  ],
  results: [
    'Все! {first_player} здобуває перемогу! <Друге місце посідає {second_player}><, за ним {third_player}>.',
    'Переможцем гонки стає {first_player}! <Друге місце посідає {second_player}><, за ним {third_player}>.'
  ]
};

export const defaultCommentatorVariable = ['commentator_name'];
export const defaultPlayerVariables = ['first_player', 'second_player', 'third_player', 'fourth_player', 'fifth_player'];
