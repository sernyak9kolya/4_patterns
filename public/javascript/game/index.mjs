import {
  switchToGamePage,
  updateRoom,
  leaveRoom,
  setReady,
  setNotReady,
  startBeforeGameTimer,
  updateBeforeGameTimer,
  startGame,
  updateGameTimer,
  endGame,
  updateComment
} from './actions.mjs';

const backToRoomsBtn = document.querySelector('#back-to-rooms-btn');
const readyBtn = document.querySelector('#game-ready-btn');
const notReadyBtn = document.querySelector('#game-not-ready-btn');

export default socket => {
  readyBtn.addEventListener('click', () => setReady(socket));
  notReadyBtn.addEventListener('click', () => setNotReady(socket));
  backToRoomsBtn.addEventListener('click', () => leaveRoom(socket));

  socket.on('UPDATE_ROOM', updateRoom);
  socket.on('JOIN_ROOM_DONE', switchToGamePage);
  socket.on('START_BEFORE_GAME_TIMER', startBeforeGameTimer);
  socket.on('UPDATE_BEFORE_GAME_TIMER', updateBeforeGameTimer);
  socket.on('START_GAME', roomId => startGame(socket)(roomId));
  socket.on('UPDATE_GAME_TIMER', updateGameTimer);
  socket.on('END_GAME', endGame);
  socket.on('UPDATE_COMMENT', updateComment);
};
