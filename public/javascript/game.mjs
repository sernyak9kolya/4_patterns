import handleUser from './user/index.mjs';
import handleRooms from './rooms/index.mjs';
import handleGame from './game/index.mjs';

const username = sessionStorage.getItem('username');

if (!username) {
  window.location.replace('/login');
}

// eslint-disable-next-line
const socket = io('', { query: { username } });

handleUser(socket)(username);
handleRooms(socket);
handleGame(socket);
