export const addClass = (element, className) => {
  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }
};

export const removeClass = (element, className) => {
  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.remove(...classNames);
  }
};

export const createElement = ({ tagName, className, attributes = {} }) => {
  const element = document.createElement(tagName);

  addClass(element, className);

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
};
