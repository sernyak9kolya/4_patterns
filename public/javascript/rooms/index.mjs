import { createRoom, updateRooms, alertExistingRoom, switchToRooms } from './actions.mjs';

const roomCreationBtn = document.querySelector('#room-creating-btn');

export default socket => {
  socket.on('UPDATE_ROOMS', rooms => updateRooms(socket)(rooms));
  socket.on('ALERT_ABOUT_EXISTING_ROOM', alertExistingRoom);
  socket.on('LEAVE_ROOM_DONE', switchToRooms);

  roomCreationBtn.addEventListener('click', () => createRoom(socket)());
};
