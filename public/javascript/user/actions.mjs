export const deleteUser = () => {
  alert('A user with this name already exists!\nPlease enter another name.');
  sessionStorage.clear('username');
  window.location.replace('/login');
};
