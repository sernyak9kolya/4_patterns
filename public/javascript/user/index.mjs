import { deleteUser } from './actions.mjs';

const usernameHeadlineElement = document.querySelector('#username');

export default socket => username => {
  usernameHeadlineElement.innerHTML = username;

  socket.on('DELETE_USER', deleteUser);
};
