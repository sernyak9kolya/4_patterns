export default class Player {
  constructor(name) {
    this.id = name;
    this.name = name;
    this.isReady = false;
    this.progress = 0;
  }

  getId() {
    return this.id;
  }

  getName() {
    return this.name;
  }

  getReady() {
    return this.isReady;
  }

  setReady(isReady) {
    this.isReady = isReady;
  }

  getProgress() {
    return this.progress;
  }

  setProgress(progress) {
    this.progress = progress;
  }
}
