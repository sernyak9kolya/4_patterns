import {
  quoteTemplates,
  defaultCommentatorVariable,
  defaultPlayerVariables
} from '../data';

const getRandomQuoteTemplate = q => q[Math.floor(Math.random() * q.length)];

const replaceQuote = (template, data = [], variables = []) => {
  let quote = template;
  let regexp = null;

  data.forEach((d, i) => {
    regexp = new RegExp(`(<(\\W*))?{${variables[i]}}((\\W*)>)?`, 'g');
    quote = quote.replace(regexp, `$2${d}$4`);
  });
  quote = quote.replace(/<.*>/, '');

  return quote;
};

export default class Commentator {
  constructor(name) {
    this.name = name;
    this.quoteTemplates = quoteTemplates;
  }

  getName() {
    return this.name;
  }

  greeting() {
    const template = getRandomQuoteTemplate(this.quoteTemplates.greeting);
    const quote = replaceQuote(template, [this.name], defaultCommentatorVariable);
    return quote;
  }

  introducing(players) {
    const template = getRandomQuoteTemplate(this.quoteTemplates.introducing);
    const quote = replaceQuote(template, [...players], defaultPlayerVariables);
    return quote;
  }

  sayGameStatus(players) {
    const template = getRandomQuoteTemplate(this.quoteTemplates.gameStatus);
    const quote = replaceQuote(template, [...players], defaultPlayerVariables);
    return quote;
  }

  sayFinishLineStatus(players) {
    const template = getRandomQuoteTemplate(this.quoteTemplates.finishLine);
    const quote = replaceQuote(template, [...players], defaultPlayerVariables);
    return quote;
  }

  sayResults(players) {
    const template = getRandomQuoteTemplate(this.quoteTemplates.results);
    const quote = replaceQuote(template, [...players], defaultPlayerVariables);
    return quote;
  }
}
