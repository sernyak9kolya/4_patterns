import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../socket/config';
import Commentator from './commentator';

export default class Room {
  constructor(name) {
    this.id = name;
    this.name = name;
    this.gameIsRunning = false;
    this.players = [];
    this.finishers = [];
    this.commentator = null;
  }

  getId() {
    return this.id;
  }

  getName() {
    return this.name;
  }

  isGameRunning() {
    return this.gameIsRunning;
  }

  gameStarted() {
    this.gameIsRunning = true;
  }

  gameFinished() {
    this.gameIsRunning = false;
  }

  isEmpty() {
    return this.players.length === 0;
  }

  isFull() {
    return this.players.length >= MAXIMUM_USERS_FOR_ONE_ROOM;
  }

  isEverybodyReady() {
    return this.players.every(player => player.getReady());
  }

  isEverybodyFinished() {
    return this.players.every(player => player.getProgress() === 100);
  }

  getPlayers() {
    return this.players;
  }

  addPlayer(player) {
    this.players.push(player);
  }

  deletePlayer(playerId) {
    this.players = this.players.filter(player => player.getId() !== playerId);
  }

  getFinishers() {
    return this.finishers;
  }

  addFinisher(player) {
    this.finishers.push(player);
  }

  clearFinishers() {
    this.finishers = [];
  }

  getCommentator() {
    return this.commentator;
  }

  setCommentator(commentatorName) {
    this.commentator = new Commentator(commentatorName);
  }

  resetCommentator() {
    this.commentator = null;
  }

  reset() {
    this.clearFinishers();
    this.resetCommentator();
    this.players.forEach(player => {
      player.setReady(false);
      player.setProgress(0);
    });
  }

  getResult() {
    const result = new Set([
      ...this.finishers,
      ...this.players.sort((first, second) => second.progress - first.progress).map(user => user.name)
    ]);
    return Array.from(result);
  }
}
