const users = new Set();

export const connectUser = socket => username => {
  if (users.has(username)) {
    socket.emit('DELETE_USER');
    return;
  }

  users.add(username);
};

export const disconnectUser = username => {
  users.delete(username);
};
