import Room from '../models/room';
import Player from '../models/player';

const rooms = new Map();

export const getRooms = () => Array.from(rooms).map(r => r[1]);

const isRoomAvailable = room => !room.isFull() && !room.isGameRunning();
export const getAvailableRooms = () => getRooms().filter(isRoomAvailable);

export const getRoomById = roomId => rooms.get(roomId);

export const addRoom = roomName => {
  rooms.set(roomName, new Room(roomName));
};

export const deleteRoom = roomId => {
  rooms.delete(roomId);
};

export const resetRoom = roomId => {
  const room = getRoomById(roomId);
  room.reset();
};

export const getResult = roomId => {
  const room = getRoomById(roomId);
  return room.getResult();
};

export const getPlayerById = (roomId, playerId) => {
  const room = getRoomById(roomId);
  const player = room.players.find(pl => pl.getId() === playerId);
  return player;
};

export const addPlayerToRoom = (roomId, playerName) => {
  const room = getRoomById(roomId);
  const player = new Player(playerName);
  room.addPlayer(player);
};

export const deletePlayerFromRoom = (roomId, playerId) => {
  const room = getRoomById(roomId);
  room.deletePlayer(playerId);

  if (room.isEmpty()) {
    deleteRoom(roomId);
  }
};

export const setPlayersReady = (roomId, playerId, isReady) => {
  const player = getPlayerById(roomId, playerId);
  player.setReady(isReady);
};
