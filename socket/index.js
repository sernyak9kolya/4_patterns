import { connectUser, disconnectUser } from '../store/users';
import handleRooms, { getCurrentRoomId, leaveRoom } from './rooms';
import handleGame from './game';

export default io => {
  io.on('connection', socket => {
    const { username } = socket.handshake.query;

    connectUser(socket)(username);
    handleRooms(io, socket);
    handleGame(io, socket);

    const disconnecting = () => {
      const roomId = getCurrentRoomId(socket);
      if (roomId) {
        leaveRoom(io, socket)(roomId);
      }
    };

    const disconnect = () => {
      disconnectUser(username);
    };

    socket.on('disconnecting', disconnecting);
    socket.on('disconnect', disconnect);
  });
};
