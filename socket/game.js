import { SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME, SECONDS_TO_UPDATE_COMMENT } from './config';
import { texts } from '../data';
import { updateEveryOnesRooms, updateActiveRoom } from './rooms';
import { getRoomById, getPlayerById, resetRoom } from '../store/rooms';

const beforeGameTimers = new Map();
const gameTimers = new Map();

const getTextIndex = () => Math.floor(Math.random() * texts.length);

const updateComment = io => (roomId, comment) => io.to(roomId).emit('UPDATE_COMMENT', comment);

const makeComment = io => (roomId, typeOfComment) => {
  const room = getRoomById(roomId);
  const commentator = room.getCommentator();
  const result = room.getResult();
  const comment = commentator[typeOfComment](result);
  updateComment(io)(roomId, comment);
};

const makeCommentOfIntroducing = io => roomId => {
  makeComment(io)(roomId, 'introducing');
};

const makeCommentOfGameStatus = io => roomId => {
  makeComment(io)(roomId, 'sayGameStatus');
};

const makeCommentOfFinishLine = io => roomId => {
  makeComment(io)(roomId, 'sayFinishLineStatus');
};

const makeCommentOfResults = io => roomId => {
  makeComment(io)(roomId, 'sayResults');
};

const clearGameTimer = roomId => {
  clearInterval(gameTimers.get(roomId));
  gameTimers.delete(roomId);
};

const clearBeforeGameTimer = roomId => {
  clearInterval(beforeGameTimers.get(roomId));
  beforeGameTimers.delete(roomId);
};

const endGame = io => roomId => {
  const room = getRoomById(roomId);
  clearGameTimer(roomId);
  makeCommentOfResults(io)(roomId);
  room.gameFinished();
  io.to(room.id).emit('END_GAME');
  resetRoom(roomId);
  updateActiveRoom(io)(roomId);
  updateEveryOnesRooms(io);
};

const startGame = io => roomId => {
  const ONE_SECOND_IN_MS = 1000;
  let seconds = SECONDS_FOR_GAME;
  let timer = null;

  const updateGameTimer = () => {
    io.to(roomId).emit('UPDATE_GAME_TIMER', seconds);

    if (seconds <= 0) {
      endGame(io)(roomId);
      return;
    }
    if (seconds % SECONDS_TO_UPDATE_COMMENT === 0) {
      makeCommentOfGameStatus(io)(roomId);
    }
    seconds -= 1;
  };

  io.to(roomId).emit('START_GAME', roomId);

  updateGameTimer();
  timer = setInterval(updateGameTimer, ONE_SECOND_IN_MS);
  gameTimers.set(roomId, timer);

  makeCommentOfIntroducing(io)(roomId);

  updateEveryOnesRooms(io);
};

const setPlayersProgress = io => (roomId, playerId, progress) => {
  const FULL_PROGRESS = 100;
  const FINISH_LINE_PROGRESS = 85;
  const room = getRoomById(roomId);
  const player = getPlayerById(roomId, playerId);
  player.setProgress(progress);

  if (player.getProgress() === FULL_PROGRESS) {
    room.addFinisher(player.name);
  }

  if (player.getProgress() >= FINISH_LINE_PROGRESS - 5
    && player.getProgress() <= FINISH_LINE_PROGRESS + 5) {
    makeCommentOfFinishLine(io)(roomId);
  }

  if (room.isEverybodyFinished()) {
    endGame(io)(roomId);
  }
};

export const startBeforeGameTimer = io => roomId => {
  const textIndex = getTextIndex();
  const ONE_SECOND_IN_MS = 1000;
  let seconds = SECONDS_TIMER_BEFORE_START_GAME;
  let timer = null;

  const updateBeforeGameTimer = () => {
    io.to(roomId).emit('UPDATE_BEFORE_GAME_TIMER', seconds);

    if (seconds <= 0) {
      clearBeforeGameTimer(roomId);
      startGame(io)(roomId);
    }

    seconds -= 1;
  };

  io.to(roomId).emit('START_BEFORE_GAME_TIMER', textIndex);

  updateBeforeGameTimer();
  timer = setInterval(updateBeforeGameTimer, ONE_SECOND_IN_MS);
  beforeGameTimers.set(roomId, timer);

  const room = getRoomById(roomId);
  room.gameStarted();

  room.setCommentator('Олексій Попов');
  const comment = room.commentator.greeting();
  updateComment(io)(roomId, comment);

  updateEveryOnesRooms(io);
};

const setProgress = io => ({ roomId, playerId, progress }) => {
  setPlayersProgress(io)(roomId, playerId, progress);
  updateActiveRoom(io)(roomId);
};

export default (io, socket) => {
  socket.on('SET_PROGRESS', data => setProgress(io)(data));
};
