import { startBeforeGameTimer } from './game';
import {
  getAvailableRooms,
  getRoomById,
  addRoom,
  addPlayerToRoom,
  deletePlayerFromRoom,
  setPlayersReady
} from '../store/rooms';

export const getCurrentRoomId = socket => Object.keys(socket.rooms).find(room => !!getRoomById(room));

export const updateMyRooms = socket => {
  const rooms = getAvailableRooms();
  socket.emit('UPDATE_ROOMS', Array.from(rooms));
};
export const updateEveryOnesRooms = io => {
  const rooms = getAvailableRooms();
  io.emit('UPDATE_ROOMS', Array.from(rooms));
};
export const updateActiveRoom = io => roomId => {
  const room = getRoomById(roomId);
  io.to(roomId).emit('UPDATE_ROOM', room);
};

const setReady = io => ({ roomId, playerId, isReady }) => {
  setPlayersReady(roomId, playerId, isReady);

  const activeRoom = getRoomById(roomId);
  if (activeRoom.isEverybodyReady()) {
    startBeforeGameTimer(io)(roomId);
  }

  updateActiveRoom(io)(roomId);
};

const joinRoom = (io, socket) => roomId => {
  const { username } = socket.handshake.query;

  socket.join(roomId, () => {
    addPlayerToRoom(roomId, username);

    socket.emit('JOIN_ROOM_DONE', roomId);

    updateActiveRoom(io)(roomId);
    updateEveryOnesRooms(io);
  });
};

const createRoom = (io, socket) => roomName => {
  const roomExists = Boolean(getRoomById(roomName));

  if (roomExists) {
    socket.emit('ALERT_ABOUT_EXISTING_ROOM');
    return;
  }

  addRoom(roomName);
  joinRoom(io, socket)(roomName);
};

export const leaveRoom = (io, socket) => roomId => {
  const { username } = socket.handshake.query;
  socket.leave(roomId, () => {
    deletePlayerFromRoom(roomId, username);

    socket.emit('LEAVE_ROOM_DONE');

    updateActiveRoom(io)(roomId);
    updateEveryOnesRooms(io);

    const leavedRoom = getRoomById(roomId);
    if (leavedRoom && !leavedRoom.isGameRunning() && leavedRoom.isEverybodyReady()) {
      startBeforeGameTimer(io)(roomId);
    }
  });
};

export default (io, socket) => {
  updateMyRooms(socket);

  socket.on('CREATE_ROOM', roomName => createRoom(io, socket)(roomName));
  socket.on('JOIN_ROOM', roomId => joinRoom(io, socket)(roomId));
  socket.on('LEAVE_ROOM', roomId => leaveRoom(io, socket)(roomId));
  socket.on('SET_READY', data => setReady(io)(data));
};
